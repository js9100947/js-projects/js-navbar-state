let isMenuOpen = false;

function toggleMenuState() {
    isMenuOpen = !isMenuOpen;
    console.log(isMenuOpen ? "menu is open" : "menu is closed");

    document.getElementById('menu-icon').innerHTML = isMenuOpen ? "close" : "menu";
    document.getElementById('menu').classList.toggle("display-block");
}